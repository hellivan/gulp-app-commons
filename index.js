'use strict';



const Clean = require('./tasks/clean');
const Serve = require('./tasks/serve');
const Build = require('./tasks/build');
const Watch = require('./tasks/watch');

const Server = require('./tasks/server/index');

const ClientPrepareIndex = require('./tasks/client/prepareIndex');
const ClientFonts = require('./tasks/client/fonts');
const ClientAssets = require('./tasks/client/assets');
const ClientLocales = require('./tasks/client/locales');
const ClientStyles = require('./tasks/client/styles');
const ClientFavicon = require('./tasks/client/favicon');

const _ = require('lodash');

const defaultBases = {
    serve: '.tmp',
    dist: 'dist',
    server: 'server',
    client: 'client'
};


class AppTasks{
    constructor(gulp, bases, packageFile){
        this.gulp = gulp;
        this.bases = _.merge({}, defaultBases, bases);
        this.packageFile = packageFile;

        Server.register(gulp, this.bases);

        ClientPrepareIndex.register(gulp, this.bases);
        ClientAssets.register(gulp, this.bases);
        ClientLocales.register(gulp, this.bases);
        ClientStyles.register(gulp, this.bases);
        ClientFonts.register(gulp, this.bases);
        ClientFavicon.register(gulp, this.bases);

        Clean.register(gulp, this.bases);
        Watch.register(gulp, this.bases);
    }
    
    
    registerServe(taskName){
        taskName = taskName || 'serve';
        Serve.register(this.gulp, this.bases, taskName);

    }
    
    registerBuild(taskName){
        taskName = taskName || 'build';
        Build.register(this.gulp, this.bases, taskName);
    }

}


module.exports.AppTasks = AppTasks;
