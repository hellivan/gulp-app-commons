'use strict';
const runSequence = require('run-sequence');

module.exports.register = function(gulp, bases, taskName){

    gulp.task('package.json:dist', function(){
        return gulp.src('package.json')
            .pipe(gulp.dest(bases.dist));
    });


    gulp.task('build:serve', ['clean:serve'], function(cb){
        runSequence.use(gulp)(['client.prepareIndex:serve', 'client.styles:serve'], cb);
    });


    gulp.task('build:dist', ['clean:dist'], function(cb){
        runSequence.use(gulp)([
            'client.prepareIndex:dist',
            'client.styles:dist',
            'client.locales:dist',
            'client.assets:dist',
            'client.fonts:dist',
            'client.favicon:dist',
            'server:dist',
            'package.json:dist'
        ], cb);
    });


    gulp.task(taskName, ['build:dist']);
};
