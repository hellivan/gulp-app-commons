'use strict';

const del = require('del');

module.exports.register = function(gulp, bases){

    gulp.task('clean:serve', function () {
        return del([bases.serve]);
    });

    gulp.task('clean:dist', function () {
        return del([bases.dist]);
    });

    gulp.task('clean', ['clean:serve', 'clean:dist']);

};

