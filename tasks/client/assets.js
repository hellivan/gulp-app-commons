'use strict';

module.exports.register = function (gulp, bases) {

    function collectAssets(){
        return gulp.src([
            `${bases.client}/assets/**`,
            `!${bases.client}/assets/{font,fonts}/**`
        ]);
    }

    gulp.task('client.assets:dist', function () {
        return collectAssets()
            .pipe(gulp.dest(`${bases.dist}/public/assets/`));
    });

};
