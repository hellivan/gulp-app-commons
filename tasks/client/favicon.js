'use strict';

module.exports.register = function (gulp, bases) {

    gulp.task('client.favicon:dist', function () {
        return gulp.src(`${bases.client}/favicon.ico`)
            .pipe(gulp.dest(`${bases.dist}/public`));
    });

};