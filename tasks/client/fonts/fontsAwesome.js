'use strict';

module.exports.register = function (gulp, bases) {

    function collectFonts(){
        return gulp.src(`${bases.client}/**/font-awesome/fonts/**/*`);
    }

    gulp.task('client.fontsAwesome:dist', function () {
        return collectFonts()
            .pipe(gulp.dest(`${bases.dist}/public`));
    });

};
