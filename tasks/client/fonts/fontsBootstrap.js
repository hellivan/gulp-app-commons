'use strict';

module.exports.register = function (gulp, bases) {

    function collectFonts(){
        return gulp.src(
            [
                `${bases.client}/**/bootstrap-sass/assets/fonts/**/*`,
                `${bases.client}/**/bootstrap/fonts/**/*`
            ]);
    }

    gulp.task('client.fontsBootstrap:dist', function () {
        return collectFonts()
            .pipe(gulp.dest(`${bases.dist}/public`));
    });

};
