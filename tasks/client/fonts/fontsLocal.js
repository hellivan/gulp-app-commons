'use strict';

module.exports.register = function (gulp, bases) {

    function collectFonts(){
        return gulp.src(`${bases.client}/assets/{font,fonts}/**/*.{ttf,otf}`);
    }

    gulp.task('client.fontsLocal:dist', function () {
        return collectFonts()
            .pipe(gulp.dest(`${bases.dist}/public/assets/`));
    });

};
