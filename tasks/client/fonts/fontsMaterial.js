'use strict';

module.exports.register = function (gulp, bases) {

    function collectFonts(){
        return gulp.src(`${bases.client}/**/material-design-icons/iconfont/**/*`);
    }

    gulp.task('client.fontsMaterial:dist', function () {
        return collectFonts()
            .pipe(gulp.dest(`${bases.dist}/public`));
    });

};
