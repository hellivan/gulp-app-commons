'use strict';

const FontsAwesome = require('./fontsAwesome');
const FontsBootstrap = require('./fontsBootstrap');
const FontsMaterial = require('./fontsMaterial');
const FontsLocal = require('./fontsLocal');

module.exports.register = function (gulp, bases) {

    FontsBootstrap.register(gulp, bases);
    FontsLocal.register(gulp, bases);
    FontsAwesome.register(gulp, bases);
    FontsMaterial.register(gulp, bases);

    gulp.task('client.fonts:dist', [
        'client.fontsBootstrap:dist',
        'client.fontsAwesome:dist',
        'client.fontsMaterial:dist',
        'client.fontsLocal:dist'
    ]);

};
