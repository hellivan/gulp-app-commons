'use strict';

module.exports.register = function (gulp, bases) {

    function collectLocales(){
        return gulp.src(`${bases.client}/**/angular-i18n/angular-locale*.js`);
    }

    gulp.task('client.locales:dist', function () {
        return collectLocales()
            .pipe(gulp.dest(`${bases.dist}/public`));
    });

};
