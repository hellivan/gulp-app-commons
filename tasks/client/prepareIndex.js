'use strict';

const wiredep = require('wiredep').stream;
const inject = require('gulp-inject');
const useref = require('gulp-useref');
const print = require('gulp-print');
const gulpif = require('gulp-if');
const uglify = require('gulp-uglify');
const cssmin = require('gulp-cssmin');
const ngAnnotate = require('gulp-ng-annotate');

module.exports.register = function (gulp, bases) {

    function injectBower() {
        return wiredep({
            exclude: [
                /bootstrap-sass-official/,
                /bootstrap.js/,
                /json3/,
                /es5-shim/,
                /bootstrap.css/,
                /font-awesome.css/
            ],
            ignorePath: `${bases.client}/bower_components/please-wait`
        });
    }

    function injectJs() {
        return inject(gulp.src([
            `${bases.client}/app/**/*.js`,
            `!${bases.client}/app/app.js`,
            `!${bases.client}/app/overviewApp.js`
        ], {read: false}), {
            relative: true,
            starttag: '<!-- inject:js -->',
            endtag: '<!-- endinject -->'
        });
    }

    function injectPleaseWait() {
        return inject(gulp.src([
            `${bases.client}/bower_components/please-wait/build/please-wait.min.js`
        ], {read: false}), {
            relative: true,
            starttag: '<!-- inject:please-wait -->',
            endtag: '<!-- endinject -->'
        });
    }


    function prepareIndexHtml() {
        return gulp.src(`${bases.client}/index.html`)
            .pipe(injectBower())
            .pipe(injectJs())
            .pipe(injectPleaseWait());
    }


    gulp.task('client.prepareIndex:serve', () => {
        return prepareIndexHtml()
            .pipe(gulp.dest(bases.serve));
    });

    gulp.task('client.prepareIndex:dist', () => {
        return prepareIndexHtml()
            .pipe(useref())
	        .pipe(gulpif('*.css', cssmin()))
	        .pipe(gulpif('*.js', ngAnnotate()))
	        .pipe(gulpif('*.js', uglify()))
            .pipe(gulp.dest(`${bases.dist}/public`));
    });

};
