'use strict';
const cssmin = require('gulp-cssmin');
const autoprefixer = require('gulp-autoprefixer');
const inject = require('gulp-inject');
const sass = require('gulp-sass');


function addVendorPrefixes(){
    return autoprefixer({
        browsers: ['last 3 version'],
        expand: true
    });
}



module.exports.register = function (gulp, bases) {

    function prepareStyles(){
        return gulp.src(`${bases.client}/app/{app,cVendors}.scss`)
            .pipe(compileScss())
            .pipe(addVendorPrefixes())
            .pipe(cssmin());
    }

    function compileScss(){
        return sass({
            outputStyle: 'expanded',
            precision: 10,
            compass: false,
            loadPath: [
                `${bases.client}/bower_components`,
                `${bases.client}/app`
            ]
        }).on('error', sass.logError);
    }


    gulp.task('client.styles:serve', function () {
        return prepareStyles()
            .pipe(gulp.dest(`${bases.serve}/app/`));
    });

    gulp.task('client.styles:dist', function () {
        return prepareStyles()
            .pipe(gulp.dest(`${bases.dist}/public/app/`));
    });

};
