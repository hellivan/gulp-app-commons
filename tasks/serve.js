'use strict';
const runSequence = require('run-sequence');

module.exports.register = function(gulp){

    gulp.task('serve', ['build:serve'], function(cb){
        runSequence.use(gulp)('watch', cb);
    });

};
