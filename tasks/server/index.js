'use strict';


module.exports.register = function (gulp, bases) {

    function collectFiles(){
        return gulp.src([
            `${bases.server}/**/*`
        ]);
    }

    gulp.task('server:dist', function () {
        return collectFiles()
            .pipe(gulp.dest(`${bases.dist}/${bases.server}`));
    });

};
