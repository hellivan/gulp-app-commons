'use strict';

module.exports.register = function(gulp, bases, pkg){

    gulp.task('watch', function(){
        // gulp.watch(bases.server + '/**/*.js', ['scripts:serve']);
        //
        //
        // gulp.watch(bases.client + '/**/*.js', ['scripts:serve']);


        gulp.watch(bases.client + '/**/*.scss', ['client.styles:serve']);
	
	gulp.watch(bases.client + '/index.html', ['client.prepareIndex:serve']);
    });

};
